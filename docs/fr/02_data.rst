Cahier des charges - contraintes de conformité des données
==========================================================

Le jeu de donnée qui est le modèle de votre base/cahier des charges,
vous servira pour la vérification des plans, mais également pour vos
prestataires réalisant le plan.

Nous allons réaliser un cahier des charges (très simple), pour de la
fibre optique.

Couche des câbles
-----------------

Schéma et nom de la couche : ``rsx_tic.tic_cable``

Représentation du réseau de télécom.

- Les arcs possèdent un nœud à chacune de leurs extrémités.
- Le nœud peut être commun à plusieurs tronçons.
- Les objets peuvent être situés sur un arc, mais ils ne peuvent les couper en deux

Règles de topologie
*******************

Le centre des objets de la couche ``tic_chambre`` est placé exactement sur les arcs de cette couche ``tic_cable``.

Géométrie :
   LINESTRING

Dimension :
   2

SRID :
   EPSG:3946

Champs
   ===== ======= ========== ===========
   Ordre Nom     Type       Commentaire
   ===== ======= ========== ===========
   1     fid     Integer    id unique, clé primaire
   2     geom    geometry
   3     nature  integer    Nature du matériau dans lequel est insérée la fibre
   4     caract  integer    État du fonctionnement du réseau
   5     classep varchar(1) Classe de précision
   ===== ======= ========== ===========

Clé(s) étrangère(s) :
   nature :
      1. PVC
      2. Acier/Fonte
      3. Béton
      4. Janolène

   caract :
      1. En service
      2. En attente
      3. Abandonné

   classep :
      A. classe A
      B. classe B
      C. classe C


Couche des chambres
-------------------

Schéma et nom de la couche : ``rsx_tic.tic_chambre``

Représentation des chambres du réseau de telecom.

Règles de topologie
*******************

Les objecs de ``ic_chambre`` sont exactement placés sur un nœud ou sommet de ``tic_cable``

Géométrie :
   POINT

Dimension :
   2

SRID :
   EPSG:3946

Champs
   ===== ======== ============ ===========
   Ordre Nom      Type         Commentaire
   ===== ======== ============ ===========
   1     fid      Integer      id unique, clé primaire
   2     geom     geometry
   3     typec    integer      Type de la chambre selon nomenclature FT
   4     alt_tn   numeric(6,2) Altitude IGN NGF 69 de la chambre
   5     alt_fond numeric(6,2) Altitude IGN NGF 69 du fond de la chambre
   ===== ======== ============ ===========

typec :
   0. L0
   1. L1
   2. L2
   3. L3
   4. QL4 (pour ¼ L4)
   5. L4
