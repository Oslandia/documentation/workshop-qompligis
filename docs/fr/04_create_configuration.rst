Création du fichier de configuration de conformité
==================================================

Nous allons, créer une configuration de confmité en utilisant le GeoPackage.
Le fonctionnement pour le ShapeFile et le DXF est identique.

Utiliser l’icône |newConfig| pour ouvrir l’assistant de configuration :

Sélection du jeu de données
---------------------------

.. image:: ../static/fr/configuration_dialog_1.png

1. Choisir le format du jeu de données parmi les types supportés

2. Sélectionner le jeu de données (ou le dossier dans le cas d’un jeu de shapefile(s))

3. Suivant

Configuration des contraintes
-----------------------------

Le panneau permet de configurer les contraintes attendues lors de la livraison
du plan.

.. image:: ../static/fr/configuration_dialog_2_tic_cable.png

1. Couches trouvées dans le jeu de données de référence

2. Vérifications générales pour la couche sélectionnée

3. Champs de la couche sélectionnée avec des vérifications pouvant être activées

4. Vérifications propres au type de géométrie de la couche sélectionnée

Procédez de même pour la couche tic_chambre:

.. image:: ../static/fr/configuration_dialog_2_tic_chambre.png

Création de la configuration
----------------------------

.. image:: ../static/fr/configuration_dialog_3.png

1. Choisir un chemin où enregistrer le fichier de configuration

2. Terminer

.. |newConfig| image:: ../static/newConfig.svg
