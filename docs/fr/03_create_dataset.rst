Création du jeu de données
==========================

Dans cet atelier, nous allons tester les trois formats supportés par QompliGIS.
Pour cela, nous allons d'abord créer le format GeoPackage puis exporté en
ShapeFile et DXF.

Au format GeoPackage
--------------------
Dans QGIS, nous allons créer un nouveau GeoPackage que nous appellerons qompligis.gpkg

Nous allons créer les deux tables présentées à la page précédente. Vous pouvez
également ajouter les tables auxiliaires pour les clés étrangères.
Toutefois, la vérification des contraintes n'est pas encore implémentée dans
QompliGIS, elle est laissée au fournisseur de données.

Au format ShapeFiles
--------------------

Comme indiqué dans la partie préparation des données, il est possible de
convertir n'importe quel format vers un autre grâce à QGIS (et gdal/ogr).
Dans notre cas, nous allons transformer les tables géométriques du GeoPackage
vers des fichiers ShapeFile.

En utilisant l'outil de la boîte des traîtements « Sauvegarder les entités sous »

.. image:: ../static/fr/gpkg_to_shp_simple.png

En cas de plusieurs fichiers, vous pouvez utiliser l'option « batch » -
processus de lot -

.. image:: ../static/fr/gpkg_to_shp_batch.png

Au format DXF
-------------

Il existe un outil particulier pour traîter un ensemble de couches dans un DXF.
Nous utiliserons l'outil « Exporter les couches en DXF »

.. image:: ../static/fr/gpkg_to_dxf.png

Attention, pour que l'outil puisse exporter correctement les informations, il
est nécessaire d'avoir un minimum de données dans les couches d'origine. Pour
cela, nous préconisons d'utilise le GeoPackage intégré dans data/exports.
