--
-- PostgreSQL database dump
--

-- Dumped from database version 13.5
-- Dumped by pg_dump version 13.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: rsx_tic; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA "rsx_tic";


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "postgis" WITH SCHEMA "public";


SET default_tablespace = '';

SET default_table_access_method = "heap";

--
-- Name: tic_cable; Type: TABLE; Schema: rsx_tic; Owner: -
--

CREATE TABLE "rsx_tic"."tic_cable" (
    "fid" integer NOT NULL,
    "geom" "public"."geometry"(LineString,3946),
    "nature" integer,
    "caract" integer,
    "classep" character varying(1)
);


--
-- Name: tic_cable_fid_seq; Type: SEQUENCE; Schema: rsx_tic; Owner: -
--

CREATE SEQUENCE "rsx_tic"."tic_cable_fid_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tic_cable_fid_seq; Type: SEQUENCE OWNED BY; Schema: rsx_tic; Owner: -
--

ALTER SEQUENCE "rsx_tic"."tic_cable_fid_seq" OWNED BY "rsx_tic"."tic_cable"."fid";


--
-- Name: tic_caract; Type: TABLE; Schema: rsx_tic; Owner: -
--

CREATE TABLE "rsx_tic"."tic_caract" (
    "fid" integer NOT NULL,
    "id" integer,
    "txt" character varying
);


--
-- Name: tic_caract_fid_seq; Type: SEQUENCE; Schema: rsx_tic; Owner: -
--

CREATE SEQUENCE "rsx_tic"."tic_caract_fid_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tic_caract_fid_seq; Type: SEQUENCE OWNED BY; Schema: rsx_tic; Owner: -
--

ALTER SEQUENCE "rsx_tic"."tic_caract_fid_seq" OWNED BY "rsx_tic"."tic_caract"."fid";


--
-- Name: tic_chambre; Type: TABLE; Schema: rsx_tic; Owner: -
--

CREATE TABLE "rsx_tic"."tic_chambre" (
    "fid" integer NOT NULL,
    "geom" "public"."geometry"(Point,3946),
    "typec" integer,
    "alt_tn" double precision,
    "alt_fond" double precision
);


--
-- Name: tic_chambre_fid_seq; Type: SEQUENCE; Schema: rsx_tic; Owner: -
--

CREATE SEQUENCE "rsx_tic"."tic_chambre_fid_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tic_chambre_fid_seq; Type: SEQUENCE OWNED BY; Schema: rsx_tic; Owner: -
--

ALTER SEQUENCE "rsx_tic"."tic_chambre_fid_seq" OWNED BY "rsx_tic"."tic_chambre"."fid";


--
-- Name: tic_classep; Type: TABLE; Schema: rsx_tic; Owner: -
--

CREATE TABLE "rsx_tic"."tic_classep" (
    "fid" integer NOT NULL,
    "id" character varying(1),
    "txt" character varying
);


--
-- Name: tic_classep_fid_seq; Type: SEQUENCE; Schema: rsx_tic; Owner: -
--

CREATE SEQUENCE "rsx_tic"."tic_classep_fid_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tic_classep_fid_seq; Type: SEQUENCE OWNED BY; Schema: rsx_tic; Owner: -
--

ALTER SEQUENCE "rsx_tic"."tic_classep_fid_seq" OWNED BY "rsx_tic"."tic_classep"."fid";


--
-- Name: tic_nature; Type: TABLE; Schema: rsx_tic; Owner: -
--

CREATE TABLE "rsx_tic"."tic_nature" (
    "fid" integer NOT NULL,
    "id" integer,
    "txt" character varying
);


--
-- Name: tic_nature_fid_seq; Type: SEQUENCE; Schema: rsx_tic; Owner: -
--

CREATE SEQUENCE "rsx_tic"."tic_nature_fid_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tic_nature_fid_seq; Type: SEQUENCE OWNED BY; Schema: rsx_tic; Owner: -
--

ALTER SEQUENCE "rsx_tic"."tic_nature_fid_seq" OWNED BY "rsx_tic"."tic_nature"."fid";


--
-- Name: tic_typechambre; Type: TABLE; Schema: rsx_tic; Owner: -
--

CREATE TABLE "rsx_tic"."tic_typechambre" (
    "fid" integer NOT NULL,
    "id" integer,
    "txt" character varying
);


--
-- Name: tic_typechambre_fid_seq; Type: SEQUENCE; Schema: rsx_tic; Owner: -
--

CREATE SEQUENCE "rsx_tic"."tic_typechambre_fid_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tic_typechambre_fid_seq; Type: SEQUENCE OWNED BY; Schema: rsx_tic; Owner: -
--

ALTER SEQUENCE "rsx_tic"."tic_typechambre_fid_seq" OWNED BY "rsx_tic"."tic_typechambre"."fid";


--
-- Name: tic_cable fid; Type: DEFAULT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_cable" ALTER COLUMN "fid" SET DEFAULT "nextval"('"rsx_tic"."tic_cable_fid_seq"'::"regclass");


--
-- Name: tic_caract fid; Type: DEFAULT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_caract" ALTER COLUMN "fid" SET DEFAULT "nextval"('"rsx_tic"."tic_caract_fid_seq"'::"regclass");


--
-- Name: tic_chambre fid; Type: DEFAULT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_chambre" ALTER COLUMN "fid" SET DEFAULT "nextval"('"rsx_tic"."tic_chambre_fid_seq"'::"regclass");


--
-- Name: tic_classep fid; Type: DEFAULT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_classep" ALTER COLUMN "fid" SET DEFAULT "nextval"('"rsx_tic"."tic_classep_fid_seq"'::"regclass");


--
-- Name: tic_nature fid; Type: DEFAULT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_nature" ALTER COLUMN "fid" SET DEFAULT "nextval"('"rsx_tic"."tic_nature_fid_seq"'::"regclass");


--
-- Name: tic_typechambre fid; Type: DEFAULT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_typechambre" ALTER COLUMN "fid" SET DEFAULT "nextval"('"rsx_tic"."tic_typechambre_fid_seq"'::"regclass");


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "public"."spatial_ref_sys" ("srid", "auth_name", "auth_srid", "srtext", "proj4text") FROM stdin;
\.


--
-- Data for Name: tic_cable; Type: TABLE DATA; Schema: rsx_tic; Owner: -
--

COPY "rsx_tic"."tic_cable" ("fid", "geom", "nature", "caract", "classep") FROM stdin;
2	01020000206A0F00000200000035FD4FD74A3C3E417A950BF0ADD75341F09B21B5493C3E41F6069742AFD75341	4	1	A
3	01020000206A0F000004000000F09B21B5493C3E41F6069742AFD7534144652005523C3E417EAF7FBBAFD75341A04B08B0573C3E41DF98DCF5B0D75341EDFAFD579E3C3E41DA956766B5D75341	4	1	A
4	01020000206A0F000002000000EDFAFD579E3C3E41DA956766B5D753414AE1E502A43C3E411DC62281AED75341	4	1	A
5	01020000206A0F0000030000004AE1E502A43C3E411DC62281AED75341AD12B580B33C3E41118FC55AAFD75341F273E3A2B43C3E41BCDCC546ADD75341	4	1	A
6	01020000206A0F0000040000004AE1E502A43C3E411DC62281AED7534187252BCAA73C3E41351DDE91A6D753413AA7134DA93C3E41B3496C3095D75341F445E52AA83C3E412D73853F88D75341	4	1	A
7	01020000206A0F000002000000F445E52AA83C3E412D73853F88D753410B9AC088643C3E4139AAE26587D75341	4	2	A
\.


--
-- Data for Name: tic_caract; Type: TABLE DATA; Schema: rsx_tic; Owner: -
--

COPY "rsx_tic"."tic_caract" ("fid", "id", "txt") FROM stdin;
1	1	En service
2	2	En attente
3	3	Abandonné
\.


--
-- Data for Name: tic_chambre; Type: TABLE DATA; Schema: rsx_tic; Owner: -
--

COPY "rsx_tic"."tic_chambre" ("fid", "geom", "typec", "alt_tn", "alt_fond") FROM stdin;
2	01010000206A0F0000EDFAFD579E3C3E41DA956766B5D75341	\N	545	542
3	01010000206A0F00004AE1E502A43C3E411DC62281AED75341	0	545	538
4	01010000206A0F0000F445E52AA83C3E412D73853F88D75341	0	545	538
5	01010000206A0F00000B9AC088643C3E4139AAE26587D75341	0	545	538
6	01010000206A0F0000A04B08B0573C3E41DF98DCF5B0D75341	0	545	538
7	01010000206A0F0000F09B21B5493C3E41F6069742AFD75341	0	545	538
\.


--
-- Data for Name: tic_classep; Type: TABLE DATA; Schema: rsx_tic; Owner: -
--

COPY "rsx_tic"."tic_classep" ("fid", "id", "txt") FROM stdin;
1	A	Classe A
2	B	Classe B
3	C	Classe C
\.


--
-- Data for Name: tic_nature; Type: TABLE DATA; Schema: rsx_tic; Owner: -
--

COPY "rsx_tic"."tic_nature" ("fid", "id", "txt") FROM stdin;
1	1	PVC
2	2	Acier/Fonte
3	3	Béton
4	4	Janolène
\.


--
-- Data for Name: tic_typechambre; Type: TABLE DATA; Schema: rsx_tic; Owner: -
--

COPY "rsx_tic"."tic_typechambre" ("fid", "id", "txt") FROM stdin;
1	0	L0
2	1	L1
3	2	L2
4	3	L3
5	4	QL4
6	5	L4
\.


--
-- Name: tic_cable_fid_seq; Type: SEQUENCE SET; Schema: rsx_tic; Owner: -
--

SELECT pg_catalog.setval('"rsx_tic"."tic_cable_fid_seq"', 7, true);


--
-- Name: tic_caract_fid_seq; Type: SEQUENCE SET; Schema: rsx_tic; Owner: -
--

SELECT pg_catalog.setval('"rsx_tic"."tic_caract_fid_seq"', 1, false);


--
-- Name: tic_chambre_fid_seq; Type: SEQUENCE SET; Schema: rsx_tic; Owner: -
--

SELECT pg_catalog.setval('"rsx_tic"."tic_chambre_fid_seq"', 7, true);


--
-- Name: tic_classep_fid_seq; Type: SEQUENCE SET; Schema: rsx_tic; Owner: -
--

SELECT pg_catalog.setval('"rsx_tic"."tic_classep_fid_seq"', 1, false);


--
-- Name: tic_nature_fid_seq; Type: SEQUENCE SET; Schema: rsx_tic; Owner: -
--

SELECT pg_catalog.setval('"rsx_tic"."tic_nature_fid_seq"', 1, false);


--
-- Name: tic_typechambre_fid_seq; Type: SEQUENCE SET; Schema: rsx_tic; Owner: -
--

SELECT pg_catalog.setval('"rsx_tic"."tic_typechambre_fid_seq"', 6, true);


--
-- Name: tic_cable tic_cable_pkey; Type: CONSTRAINT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_cable"
    ADD CONSTRAINT "tic_cable_pkey" PRIMARY KEY ("fid");


--
-- Name: tic_caract tic_caract_pkey; Type: CONSTRAINT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_caract"
    ADD CONSTRAINT "tic_caract_pkey" PRIMARY KEY ("fid");


--
-- Name: tic_chambre tic_chambre_pkey; Type: CONSTRAINT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_chambre"
    ADD CONSTRAINT "tic_chambre_pkey" PRIMARY KEY ("fid");


--
-- Name: tic_classep tic_classep_pkey; Type: CONSTRAINT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_classep"
    ADD CONSTRAINT "tic_classep_pkey" PRIMARY KEY ("fid");


--
-- Name: tic_nature tic_nature_pkey; Type: CONSTRAINT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_nature"
    ADD CONSTRAINT "tic_nature_pkey" PRIMARY KEY ("fid");


--
-- Name: tic_typechambre tic_typechambre_pkey; Type: CONSTRAINT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_typechambre"
    ADD CONSTRAINT "tic_typechambre_pkey" PRIMARY KEY ("fid");


--
-- Name: sidx_tic_cable_geom; Type: INDEX; Schema: rsx_tic; Owner: -
--

CREATE INDEX "sidx_tic_cable_geom" ON "rsx_tic"."tic_cable" USING "gist" ("geom");


--
-- Name: sidx_tic_chambre_geom; Type: INDEX; Schema: rsx_tic; Owner: -
--

CREATE INDEX "sidx_tic_chambre_geom" ON "rsx_tic"."tic_chambre" USING "gist" ("geom");


--
-- Name: tic_caract_id_uindex; Type: INDEX; Schema: rsx_tic; Owner: -
--

CREATE UNIQUE INDEX "tic_caract_id_uindex" ON "rsx_tic"."tic_caract" USING "btree" ("id");


--
-- Name: tic_classep_id_uindex; Type: INDEX; Schema: rsx_tic; Owner: -
--

CREATE UNIQUE INDEX "tic_classep_id_uindex" ON "rsx_tic"."tic_classep" USING "btree" ("id");


--
-- Name: tic_nature_id_uindex; Type: INDEX; Schema: rsx_tic; Owner: -
--

CREATE UNIQUE INDEX "tic_nature_id_uindex" ON "rsx_tic"."tic_nature" USING "btree" ("id");


--
-- Name: tic_typechambre_id_uindex; Type: INDEX; Schema: rsx_tic; Owner: -
--

CREATE UNIQUE INDEX "tic_typechambre_id_uindex" ON "rsx_tic"."tic_typechambre" USING "btree" ("id");


--
-- Name: tic_cable tic_cable_tic_caract_id_fk; Type: FK CONSTRAINT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_cable"
    ADD CONSTRAINT "tic_cable_tic_caract_id_fk" FOREIGN KEY ("caract") REFERENCES "rsx_tic"."tic_caract"("id");


--
-- Name: tic_cable tic_cable_tic_classep_id_fk; Type: FK CONSTRAINT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_cable"
    ADD CONSTRAINT "tic_cable_tic_classep_id_fk" FOREIGN KEY ("classep") REFERENCES "rsx_tic"."tic_classep"("id");


--
-- Name: tic_cable tic_cable_tic_nature_id_fk; Type: FK CONSTRAINT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_cable"
    ADD CONSTRAINT "tic_cable_tic_nature_id_fk" FOREIGN KEY ("nature") REFERENCES "rsx_tic"."tic_nature"("id");


--
-- Name: tic_chambre tic_chambre_tic_typechambre_id_fk; Type: FK CONSTRAINT; Schema: rsx_tic; Owner: -
--

ALTER TABLE ONLY "rsx_tic"."tic_chambre"
    ADD CONSTRAINT "tic_chambre_tic_typechambre_id_fk" FOREIGN KEY ("typec") REFERENCES "rsx_tic"."tic_typechambre"("id");


--
-- PostgreSQL database dump complete
--

